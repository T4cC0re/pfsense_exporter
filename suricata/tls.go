package suricata

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"net"
	"regexp"
	"strings"
	"time"
)

var (
	tls_conn = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pfsense_suricata_tls_total",
		Help: "The total number of TLS connections",
	}, []string{"version", "src_ip", "dest_ip", "dest_port", "sni", "iface", "ifacefriendlyname", "direction", "state"})
)

func init() {
	tls_conn.Reset()
	go func() {
		for {
			time.Sleep(5 * time.Minute)
			tls_conn.Reset()
		}
	}()
}

func extractCN(subject string) string {
	re := regexp.MustCompile("CN=([^,]+)")
	matches := re.FindStringSubmatch(subject)
	if len(matches) != 2 {
		return subject
	}

	return strings.TrimSpace(matches[1])
}

func parseSuricataTLS(suricata map[string]interface{}, src_ip string, ip net.IP, dest_port int, in_iface string, friendlyName string, direction string) {
	if tls, ok := suricata["tls"].(map[string]interface{}); ok {
		sni := strings.ToLower(stringOrDefault(tls, "sni", extractCN(stringOrDefault(tls, "subject", ip.String()))))
		//fingerprint := stringOrDefault(tls, "fingerprint", "")
		version := strings.ToLower(stringOrDefault(tls, "version", ""))
		resumed := boolOrDefault(tls, "session_resumed", false)

		if resumed {
			tls_conn.WithLabelValues(version, src_ip, ip.String(), fmt.Sprintf("%d", dest_port), sni, in_iface, friendlyName, direction, "resumed").Inc()
		} else {
			tls_conn.WithLabelValues(version, src_ip, ip.String(), fmt.Sprintf("%d", dest_port), sni, in_iface, friendlyName, direction, "new").Inc()
		}
		fmt.Printf("Logged conn: \n", version, src_ip, ip.String(), fmt.Sprintf("%d", dest_port), sni, in_iface, friendlyName, direction, "resumed")
	} else {
		fmt.Printf("Err: no tls field tls event %s\n", suricata)
	}
}
