package suricata

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/T4cC0re/pfsense_exporter/config"
	"gitlab.com/T4cC0re/pfsense_exporter/global"
	"net"
	"strings"
)

var (
	events = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pfsense_suricata_events_total",
		Help: "The total number of events",
	}, []string{"type", "iface", "ifacefriendlyname", "direction", "proto"})
)

func init() {
	events.Reset()
}

var ErrExcludedIface = errors.New("interface excluded")

func ParseSuricata(message []byte) (err error) {
	suricata := map[string]interface{}{}

	err = json.Unmarshal(message, &suricata)

	event_type := strings.ToLower(stringOrDefault(suricata, "event_type", ""))
	dest_ip := stringOrDefault(suricata, "dest_ip", "")
	src_ip := stringOrDefault(suricata, "src_ip", "")
	in_iface := stringOrDefault(suricata, "in_iface", "")
	proto := strings.ToLower(stringOrDefault(suricata, "proto", ""))
	dest_port := intOrDefault(suricata, "dest_port", 0)

	for _, excludedInterface := range excludedInterfaces {
		if in_iface == excludedInterface {
			if global.Verbose {
				fmt.Printf("dropping suricata event for excluded interface (iface: %s): %s\n", in_iface, message)
			}
			return ErrExcludedIface
		}
	}

	ip := net.ParseIP(dest_ip)

	addrs, err := net.InterfaceAddrs()

	direction := "egress"

	for _, addr := range addrs {
		_, net, err := net.ParseCIDR(addr.String())
		if err != nil {
			fmt.Printf("err: %s\n", err)
			continue
		}
		if net.Contains(ip) {
			direction = "ingress"
			break
		}
	}

	friendlyName := config.GetInterfaceFriendlyName(in_iface)
	events.WithLabelValues(event_type, in_iface, friendlyName, direction, proto).Inc()

	switch event_type {
	case "tls":
		parseSuricataTLS(suricata, src_ip, ip, dest_port, in_iface, friendlyName, direction)
	case "ssh":
		fmt.Printf("ssh: %s\n", message)
		parseSuricataSSH(suricata, src_ip, ip, dest_port, in_iface, friendlyName, direction)
	default:
		if global.Verbose {
			fmt.Printf("unknown suricata payload (type: %s): %s\n", event_type, message)
		}
	}

	return
}
