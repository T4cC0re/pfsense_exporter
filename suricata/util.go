package suricata

import (
	"fmt"
	"strconv"
)

func stringOrDefault(in map[string]interface{}, key string, def string) string {
	if val, ok := in[key].(string); ok && val != "" {
		return val
	}
	return def
}
func boolOrDefault(in map[string]interface{}, key string, def bool) bool {
	if val, ok := in[key].(bool); ok {
		return val
	}
	return def
}
func intOrDefault(in map[string]interface{}, key string, def int) int {
	if val, ok := in[key].(interface{}); ok {
		i, _ := strconv.ParseInt(fmt.Sprintf("%v", val), 10, 32)
		return int(i)
	}
	return def
}

var excludedInterfaces []string

func SetExcludedInterfaces(in []string) {
	excludedInterfaces = in
}
