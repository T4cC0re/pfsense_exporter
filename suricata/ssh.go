package suricata

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"net"
)

var (
	ssh_conn = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pfsense_suricata_ssh_total",
		Help: "The total number of SSH connections",
	}, []string{"src_ip", "dest_ip", "dest_port", "iface", "ifacefriendlyname", "direction", "client_version", "server_version"})
)

func init() {
	ssh_conn.Reset()
}

func parseSuricataSSH(suricata map[string]interface{}, src_ip string, ip net.IP, dest_port int, in_iface string, friendlyName string, direction string) {
	if ssh, ok := suricata["ssh"].(map[string]interface{}); ok {
		server_version := ""
		client_version := ""

		if client, okc := ssh["client"].(map[string]interface{}); okc {
			client_version = stringOrDefault(client, "software_version", "")
		}
		if server, oks := ssh["server"].(map[string]interface{}); oks {
			server_version = stringOrDefault(server, "software_version", "")
		}

		ssh_conn.WithLabelValues(src_ip, ip.String(), fmt.Sprintf("%d", dest_port), in_iface, friendlyName, direction, client_version, server_version).Inc()
	} else {
		fmt.Printf("Err: no ssh field ssh event: %s\n", suricata)
	}
}
