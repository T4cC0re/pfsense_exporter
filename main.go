package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/T4cC0re/pfsense_exporter/global"
	"gitlab.com/T4cC0re/pfsense_exporter/suricata"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

var logs chan []byte

var version = "source"

var excludeRegexps []*regexp.Regexp // Compiled at runtime! Do not set.
var discard = true

type arrayFlags []string

func (i *arrayFlags) String() string {
	return strings.Join(*i, " | ")
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

var excludePatterns arrayFlags
var excludeInterfaces arrayFlags

func main() {
	logs = make(chan []byte, 1024)

	fmt.Fprintf(os.Stderr, "pfsense_exporter version %s\n", version)

	flag.Var(&excludePatterns, "exclude", "Regex to exclude from parsing. Can be specified multiple times.")
	flag.Var(&excludeInterfaces, "no-iface", "Interface names to exclude from exporting suricata data. Can be specified multiple times.")
	flag.BoolVar(&global.Verbose, "verbose", false, "Enable more verbose logging")
	flag.Parse()

	for i := 0; i < 4; i++ {
		go worker()
	}

	excludeRegexps = compileExcludes(excludePatterns)
	suricata.SetExcludedInterfaces(excludeInterfaces)

	for _, exp := range excludeRegexps {
		fmt.Printf("Loaded bypass rule: /%s/\n", exp.String())
	}

	go ingestLog("/var/log/system.log")
	go ingestLog("/var/log/filter.log")

	fmt.Printf("Warming up...\n")
	time.Sleep(2 * time.Second)
	discard = false
	logs_ingested.Reset()
	logs_discarded.Reset()
	fmt.Printf(" OK!\n")

	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":2112", nil))
}

var (
	logs_ingested = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pfsense_logs_ingested_total",
		Help: "The total log lines ingested",
	}, []string{"file"})
	logs_discarded = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pfsense_logs_discarded_total",
		Help: "The total log lines discarded",
	}, []string{"file", "reason"})
)

func ingestLog(log string) {
	cmd := exec.Command("clog", "-f", log)

	reader, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}

	go func() {
		breader := bufio.NewReader(reader)

		for {
			text, _ := breader.ReadString('\n')
			if discard {
				logs_discarded.WithLabelValues(log, "discard").Inc()
				continue
			}

			logs_ingested.WithLabelValues(log).Inc()
			logs <- []byte(text)
		}
	}()

	err = cmd.Run()
	if err != nil {
		panic(err)
	}
}

func compileExcludes(patterns []string) (ret []*regexp.Regexp) {
	errors := 0

	for _, pattern := range patterns {
		regex, err := regexp.Compile(pattern)
		if err != nil {
			fmt.Println(err)
			errors++
		}
		ret = append(ret, regex)
	}

	if errors > 1 {
		panic("Error compiling excludes")
	}
	return
}

func worker() {
	fmt.Printf("Started worker\n")
	for {
		line := <-logs

		if shouldBypass(line) {
			logs_discarded.WithLabelValues("worker", "bypass").Inc()
			continue
		}

		// clog format haxx
		splits := bytes.SplitN(line, []byte(":"), 4)
		if len(splits) < 4 {
			continue
		}
		host := bytes.Join(splits[:3], []byte(":"))
		message := bytes.TrimSpace(splits[3])
		hsplits := bytes.SplitN(host, []byte(" "), 6)
		if len(hsplits) < 5 {
			continue
		}
		month := hsplits[0]
		day := hsplits[1]
		t := hsplits[2]
		host = hsplits[3]
		app := hsplits[4]
		// Sometimes day is '', because the day does not have 2 digits
		if string(day) == "" && len(hsplits) == 6 {
			day = hsplits[2]
			t = hsplits[3]
			host = hsplits[4]
			app = hsplits[5]
		}

		// no-op, but we want the variables for now.
		_ = fmt.Sprintf("month: %s day: %s t: %s host: %s app: %s \n", month, day, t, host, app)

		if bytes.HasPrefix(app, []byte("suricata")) {
			if message[0] != byte('{') {
				if global.Verbose {
					fmt.Printf("Not a suricata JSON line: %s\n", message)
				}
				logs_discarded.WithLabelValues("worker", "not_json").Inc()

				// Not a suricata JSON line
				continue
			}
			err := suricata.ParseSuricata(message)
			if err != nil && err != suricata.ErrExcludedIface {
				fmt.Printf("Err: %v\n"+
					"ErrLine: %s\n", err, message)

				logs_discarded.WithLabelValues("worker", "error").Inc()
				continue
			}
		} else {
			if global.Verbose {
				fmt.Printf("unknown app '%s', message: %s\n", app, message)
			}
			logs_discarded.WithLabelValues("worker", "unknown").Inc()
		}
	}
}

func shouldBypass(line []byte) bool {
	for _, regex := range excludeRegexps {
		if data := regex.Find(line); len(data) > 0 {
			if global.Verbose {
				fmt.Printf("bypass, regex: %s | match: %s | line: %s\n", regex, data, line)
			}
			return true
		}
	}

	return false
}
