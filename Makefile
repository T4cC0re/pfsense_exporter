SRC = $(shell find . -type f -name '*.go')
pkgver ?= $(shell git describe --always --dirty)
DEST ?= .

.PHONY: all
all: $(DEST)/pfsense_exporter

$(DEST)/pfsense_exporter: $(SRC)
	GOOS=freebsd go build -trimpath -ldflags="-w -s -X main.version=v$(pkgver) -extldflags=-Wl,-z,now,-z,relro" -o $(DEST)/$@ .