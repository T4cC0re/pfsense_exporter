package config

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

type Config struct {
	XMLName    xml.Name `xml:"pfsense"`
	Inferfaces struct {
		IFs []Interface `xml:",any"`
	} `xml:"interfaces"`
}

type Interface struct {
	Ifname       string `xml:"if"`
	FriendlyName string `xml:"descr"`
}

var cachedInterfaces = map[string]string{}
var cachedTime time.Time

func GetInterfaceFriendlyName(ifname string) string {
	if friendly, ok := GetInterfaceMap()[ifname]; ok {
		return friendly
	}
	return ifname
}

func GetInterfaceMap() map[string]string {
	if !hasChanged(cachedTime) {
		return cachedInterfaces
	}

	interfaces := map[string]string{}

	xmlFile, err := os.Open(config_file)
	if err != nil {
		fmt.Println(err)
	}

	defer xmlFile.Close()

	byteValue, _ := ioutil.ReadAll(xmlFile)
	readTime := time.Now()
	var config Config
	err = xml.Unmarshal(byteValue, &config)
	if err != nil {
		fmt.Println(err)
	}

	for _, If := range config.Inferfaces.IFs {
		interfaces[If.Ifname] = strings.ToUpper(If.FriendlyName)
	}

	cachedInterfaces = interfaces
	cachedTime = readTime

	return cachedInterfaces
}
