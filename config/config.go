package config

import (
	"crypto/sha512"
	"fmt"
	"io"
	"os"
	"time"
)

const config_file = "/conf/config.xml"

var lastHash string
var lastModification time.Time

func init() {
	fmt.Printf("%+v\n", GetInterfaceMap())
	fmt.Printf("%+v\n", lastHash)
	fmt.Printf("%+v\n", lastModification)
}

func hasChanged(since time.Time) bool {
	if !getModTime().After(since) {
		return false
	}
	lastModification = getModTime()
	lastHash = getHash()

	return true
}

func getModTime() time.Time {

	stat, err := os.Stat(config_file)
	if err != nil {
		return time.Time{}
	}

	return stat.ModTime()
}

func getHash() string {
	hash := sha512.New()
	file, err := os.Open(config_file)
	if err != nil {
		return ""
	}
	defer file.Close()

	io.Copy(hash, file)
	return fmt.Sprintf("%x", hash.Sum(nil))
}
